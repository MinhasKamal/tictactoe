# TicTacToe/CrissCross
#### Naive AI Enabled Tic-Tac-Toe Game 

It is a simple [Tic-Tac-Toe](http://minhaskamal.github.io/TicTacToe) game and you can play with the computer too! The computer has two AI-Players, having two levels of smartness. It is almost impossible to beat the hard level :sunglasses:.

The game was first created [in C](https://github.com/MinhasKamal/CoolConsoleGames/blob/master/src/CrissCrossV-2.c), then was rewritten and converted to Java. The actual name of the project was **CrissCross**.

### License
<a rel="license" href="http://www.gnu.org/licenses/gpl.html"><img alt="GNU General Public License" style="border-width:0" src="http://www.gnu.org/graphics/gplv3-88x31.png" /></a><br/>TicTacToe/CrissCross is licensed under a <a rel="license" href="http://www.gnu.org/licenses/gpl.html">GNU General Public License version-3</a>.
